libexecs (1.4-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 01 Jul 2023 10:27:25 +0200

libexecs (1.4-1) experimental; urgency=medium

  * New upstream version 1.4.
  * Change library package names following bump in SONAME.

 -- Renzo Davoli <renzo@cs.unibo.it>  Sun, 25 Jun 2023 16:31:52 +0200

libexecs (1.3-3) unstable; urgency=medium

  [ Renzo Davoli ]
  * Move the static libraries to the -dev package, instead of library package.
    + Add Breaks/Replaces against the previous version.
  * Bump Standards-Version to 4.6.2, no changes needed.

  [ Mattia Rizzolo ]
  * d/watch: drop filenamemangle rule.
  * d/rules: get rid of the base dh-make template that doesn't bring anything.

 -- Renzo Davoli <renzo@cs.unibo.it>  Sun, 25 Jun 2023 12:13:46 +0200

libexecs (1.3-2) unstable; urgency=medium

  [Debian Janitor]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on cmake.

  [Andrea Capriotti]
  * Bump Standards-Version to 4.6.1, no changes needed.
  * d/copyright: Update Debian copyright

 -- Andrea Capriotti <capriott@debian.org>  Sun, 14 Aug 2022 13:40:18 +0200

libexecs (1.3-1) unstable; urgency=medium

  [ Renzo Davoli ]
  * new tag 1.3
  * use cmake instead of autotools
  * move maintenance within the Virtualsquare team
  * add watch file
  * bump debhelper compat level to 13

  [ Mattia Rizzolo ]
  * Mark all packages as Multi-Arch:same (from the m-a hinter).

  [ Andrea Capriotti ]
  * d/control: Bump Standard version to 4.5.1

 -- Andrea Capriotti <capriott@debian.org>  Mon, 18 Jan 2021 19:41:36 +0100

libexecs (1.2-1) unstable; urgency=medium

  * new tag 1.2.
  * fix management of debian packaging on git

 -- Renzo Davoli <renzo@cs.unibo.it>  Sat, 05 Jan 2019 14:43:54 +0100

libexecs (1.1-1) unstable; urgency=medium

  * New upstream release. (Closes: #873675)
  * Update Standards-Version to 4.0.0.
  * Add missing dependency in libexecs-dev. (Closes: #875421)

 -- Giulia Cantini <giuliacantini3@gmail.com>  Sat, 14 Oct 2017 18:00:04 +0200

libexecs (1.0-1) unstable; urgency=low

  * Initial release. (Closes: #859737)

 -- Giulia Cantini <giuliacantini3@gmail.com>  Mon, 13 Mar 2017 17:00:52 +0100
